'use strict';

/**
 * This controller responsible to fetch the data and contains logic to config the grid as per requirements
 *
 */
jeansCo.controller('SalesController',['$scope', '$http','salesService', '$interval','$filter', 'uiGridGroupingConstants','uiGridConstants',
    function ($scope, $http,salesService, $interval,$filter, uiGridGroupingConstants,uiGridConstants ) {

    var vm = this;

    // grid configuration
    $scope.gridOptions = {
        enableFiltering: true, // enable filtering
        treeRowHeaderAlwaysVisible: false,
        showColumnFooter: true, // shows column footers
        enableColumnResizing: true, // enable resizing
        columnDefs: [
            {name: 'orderDate', caption:'Order Date', cellFilter: 'date', type: 'date', width: '15%'},
            {name: 'deliveryCountry',width: '15%',type:'string'},
            {name: 'manufacturer',  width: '20%',type:'string'},
            {name: 'gender', width: '10%',type:'string'},
            {name: 'size', width: '10%' ,type:'string'},
            {name: 'color',width: '10%',type:'string'},
            {name: 'style',width: '10%',type:'string'},
            {name: 'count',width: '10%',type:'number' },
            {name: 'month',width:'10%',type:'string' }
        ],
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
        }
    };

     /**
     * function is called when select the report
     *
     */
      $scope.selectReport = function(id){
          $scope.gridApi.grouping.clearGrouping();
          $scope.gridApi.grid.columns[9].visible = false;
          if(id===1){
              vm.applyGroupingManuByGenAndCountry();
          }
          else if(id===2){
              vm.applyGroupingSizeByCountry();
          }
          else if(id===3){
              vm.applyGroupingMonthByCountry();
          }
          $scope.gridApi.grid.refresh();
          $scope.gridApi.treeBase.expandAllRows();

    };

    /**
     * function to configure grouping by Gender and Country
     *
     */
    vm.applyGroupingManuByGenAndCountry = function () {
        $scope.gridApi.grouping.groupColumn('gender');
        $scope.gridApi.grouping.groupColumn('deliveryCountry');
        $scope.gridApi.grouping.aggregateColumn('count', uiGridGroupingConstants.aggregation.SUM);

        var genderColumn = vm.getColumn('gender');
        genderColumn.sort.priority = 1;
        var countryColumn = vm.getColumn('deliveryCountry');
        countryColumn.sort.priority = 2;
        var countColumn = vm.getColumn('count');
        countColumn.sort.priority = 0;
        countColumn.sort.direction = uiGridConstants.DESC;
    };

    /**
     * function to configure grouping byCountry
     *
     */
    vm.applyGroupingSizeByCountry = function () {
        $scope.gridApi.grouping.groupColumn('deliveryCountry');
        $scope.gridApi.grouping.aggregateColumn('count', uiGridGroupingConstants.aggregation.SUM);

        var countryColumn = vm.getColumn('deliveryCountry');
        countryColumn.sort.priority = 1;
        var countColumn = vm.getColumn('count');
        countColumn.sort.priority = 0;
        countColumn.sort.direction = uiGridConstants.DESC;

    };

    /**
     * function to configure grouping by Month and Country
     *
     */
    vm.applyGroupingMonthByCountry = function () {
        $scope.gridApi.grid.columns[9].visible = true;
        $scope.gridApi.grouping.groupColumn('month');
        $scope.gridApi.grouping.groupColumn('deliveryCountry');
        $scope.gridApi.grouping.aggregateColumn('count', uiGridGroupingConstants.aggregation.SUM);

        var monthColumn = vm.getColumn('month');
        monthColumn.sort.priority = 1;
        monthColumn.sort.direction = uiGridConstants.DESC;

        var countryColumn = vm.getColumn('deliveryCountry');
        countryColumn.sort.priority = 3;
        var countColumn = vm.getColumn('count');
        countColumn.sort.priority = 2;
        countColumn.sort.direction = uiGridConstants.DESC;

    };

    /**
     * function to get column by name
     *
     */
    vm.getColumn = function(name){
        for(var i=0; i< $scope.gridApi.grid.columns.length; i++){
            if($scope.gridApi.grid.columns[i].name === name){
                return $scope.gridApi.grid.columns[i];
            }
        }
        return $scope.gridApi.grid.columns[1];
    };

    // function to load sales data from sales service
    vm.getSalesData = function(){
      salesService.getSalesData()
          .success(function (data) {
              for (var i = 0; i < data.length; i++) {
                  var orderDate = new Date(data[i].orderDate);
                  data[i].month = $filter('date')(orderDate, 'MMMM');
              }
              $scope.gridOptions.data = data;
          })
         .error(function (error) {
            console.log('Unable to load sales data: ' + error.message);
         });

    };

    // load sales data
    vm.getSalesData();

    }]);
