'use strict';

describe('JeansCo module', function() {
    beforeEach(module('JeansCo'));

    describe('sales-view-comp directive', function() {
        it('should print salesGrid', function() {
            module(function($provide) {
                $provide.value('id', 'TEST_ID');
            });
            inject(function($compile, $rootScope) {
                var element = $compile('<sales-view-comp></sales-view-comp>')($rootScope);
                expect(element).isPrototypeOf('div');
            });
        });
    });
});
