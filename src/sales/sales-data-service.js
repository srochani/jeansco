'use strict';

/**
 * This service responsible to fetch the sales data
 *
 */
jeansCo.service('salesService',['$http',function($http){

    this.getSalesData = function(){
       return $http.get('sample/salesdata.json');
    }
}]);