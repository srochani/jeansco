# `Online Jeans Co.` — AngularJS apps

### Description

This project is a online Jeans co. static web app developed in AngularJS application and
is preconfigured to install the Angular, Material-design and angular-ui-grid framework.
Here endpoint is returning the yearly sales data as per sampele format.

The angular-ui-grid has nice feature to display data in multiple groups, I have used this display
data as per requirements so it will help sells team to make purchasing decision.

* The top selling manufacturers by gender and country - supported
* The top selling sizes by country - supported 
* The top selling months globally and by country - supported 

### Install Dependencies and how to open in brower

```
npm install
```

Behind the scenes this will also call `bower install`. After that, you should find out that you have
two new folders in your project.

* `node_modules` - contains the npm packages for the tools we need
* `bower_components` - contains the Angular framework files

### Run the Application

We have preconfigured the project with a simple development web server. The simplest way to start
this server is:

```
npm start
```

Now browse to the app at [`localhost:8000/index.html`][local-app-url].

### Sample data file location
src/sample/salesdata.json

## Testing

There are two kinds of tests in the `JeansCo` application: Unit tests.

### Running Unit Tests

The `JeansCo` app comes preconfigured with unit tests. These are written in [Jasmine][jasmine],
which we run with the [Karma][karma] test runner. I've provide a Karma configuration file to run them.

* The configuration is found at `karma.conf.js`.
* The unit tests are found next to the code they are testing and have an `_test.js` suffix (e.g.
  `*_test.js`).

To run the unit tests is to use the supplied npm script:

```
npm test
```

You can also ask Karma to do a single run of the tests and then exit. 

```
npm run test-single-run
```

### Reasons for your choice of data format and protocol for the API.
I have use the JSON data format which are return by REST API end point.


### Running the App during Development

The `JeansCo` project comes preconfigured with a local development web server. It is a Node.js
tool called [http-server][http-server]. You can start this web server with `npm start`, but you may
choose to install the tool globally:

```
sudo npm install -g http-server
```

Then you can start your own development web server to serve static files from a folder by running:

```
http-server -a localhost -p 8000
```

Alternatively, you can choose to configure your own web server, such as Apache or Nginx. Just
configure your server to serve the files under the `src/` directory.
